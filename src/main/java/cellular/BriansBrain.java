package cellular;

import datastructure.IGrid;
import java.util.Random;
import datastructure.CellGrid;


public class BriansBrain implements CellAutomaton {


    IGrid Brian;

    public BriansBrain(int rows, int columns) {
		Brian = new CellGrid(rows, columns, CellState.ALIVE);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        return Brian.get(row, column);

    }
    private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;

		for (int i = row-1;i<=row+1; i++){
			for (int j = col-1; j<=col+1; j++){
				if (i<0 || j<0 || i>=numberOfRows() || j>=numberOfColumns() || (i==row && j==col)){
					continue;
				}
				else if (getCellState(i, j) == state){
					counter++;
				}
			}
		}
		return counter;
	}

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				if (random.nextBoolean()) {
					Brian.set(row, col, CellState.ALIVE);
				} else  {
					Brian.set(row, col, CellState.DEAD);
				}
                
			}
		}
        
    }

    @Override
    public void step() {
        IGrid nextBrain = Brian.copy();
		
		for (int i=0; i<numberOfRows();i++) {
			for (int j=0; j<numberOfColumns();j++){
				nextBrain.set(i, j, getNextCell(i, j));
			}
		}
		Brian = nextBrain;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row, col);
        if (state == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (state == CellState.DYING){
            return CellState.DEAD;
        }
        else {
            if (countNeighbors(row, col, CellState.ALIVE)==2){
                return CellState.ALIVE;
            }
            else {
                return state;
            }
        }
    }

    @Override
    public int numberOfRows() {
        return Brian.numRows();
    }

    @Override
    public int numberOfColumns() {
        return Brian.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return Brian;
    }
    
}
